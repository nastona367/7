from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .models import *

def main(request):
    return render(request, 'pages/two_buttons.html')

def new_reg(request):
    return render(request, 'pages/main.html')

def reg(request):
    name = request.POST['name']
    year = request.POST['year']
    email = request.POST['email']
    password = request.POST['password']
    if name or year:
        Games.objects.create(name=name, year=year)
    User.objects.create(email=email, password=password)
    game = Games.objects.all()
    return render(request, 'pages/1.html', {'game': game})

def avt(request):
    return render(request, 'pages/main1.html')

def new_avt(request):
    email = request.POST['email']
    password = request.POST['password']
    for i in User.objects.all():
        if email == i.email and password == i.password:
            game = Games.objects.all()
            return render(request, 'pages/1.html', {'game': game})
    return render(request, 'pages/main1.html')

def view_posts(request, id):
    Games.objects.filter(pk=id).delete()
    game = Games.objects.all()
    return render(request, 'pages/1.html', {'game': game})

def wot_view_posts(request):
    id = request.POST['id']
    g = request.POST['game']
    if id:
        game = Games.objects.filter(pk=id)
    elif g:
        game = Games.objects.filter(name=g)
    return render(request, 'pages/main11.html', {'game': game})