from django.urls import path
from .views import *

urlpatterns = [
    path('', main, name='main'),
    path('new_reg/', new_reg, name='new_reg'),
    path('reg/', reg, name='reg'),
    path('avt/', avt, name='avt'),
    path('new_avt/', new_avt, name='new_avt'),
    path('<int:id>/', view_posts, name='view_posts'),
    path('wot/', wot_view_posts, name='wot_view_posts'),]

