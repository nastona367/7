from django.db import models

class User(models.Model):
    email = models.CharField(max_length=20, verbose_name="Пошта", blank=True)
    password = models.CharField(max_length=20, verbose_name="Пароль", blank=True)

class Games(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=20, verbose_name="Назва гри", blank=True)
    year = models.DateField(verbose_name="Дата створення", blank=True)
