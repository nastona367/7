from django import forms
from django.contrib import admin
from .models import *


class GamesAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'year')

class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'password')


admin.site.register(Games, GamesAdmin)
admin.site.register(User, UserAdmin)
